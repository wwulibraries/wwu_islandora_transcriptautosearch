# Overview
When you search in Islandora, and are taken to a page that uses the PDF.js viewer, the term you searched for is not highlighted in the Transcript tab.
This module attempts to fix that problem by repeating the search within the Transcript tab.

# Requirements
Islandora 7.1.12+ or  "Enable search navigation block" must be enabled in your Islandora settings:
admin/islandora/search/islandora_solr/settings

as described in https://wiki.duraspace.org/display/ISLANDORA/Islandora+Solr+Settings
and shown here:
 ![screenshot of settings](display-settings.png)

# Installation
- navigate to your modules directory (usually /var/www/drupal/sites/all/modules/)
- clone this repository into that folder (e.g, 
git clone https://bitbucket.org/wwulibraries/wwu_islandora_pdfautosearch.git)
- drush en wwu_islandora_transcriptautosearch

# Author: 
David Bass @ WWU

# License: 
MIT