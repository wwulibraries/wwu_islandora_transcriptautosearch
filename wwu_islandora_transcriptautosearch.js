// https://bitbucket.org/wwulibraries/wwu_islandora_transcriptautosearch/src/master/
jQuery( document ).ready(function() {

    var theURL =  window.location.pathname;
    var isTranscript = theURL.indexOf('/transcript');

    if (isTranscript !== -1 && location.search) {
        var urlParams = new URLSearchParams(window.location.search);
        var searchFor = urlParams.get('search');
        console.log('searching the transcript for ' + searchFor);
        // search through the Transcript tab for the searchFor query, allowing one or more spaces (or other whitespace characters) between terms (regex); thanks to https://stackoverflow.com/a/15472787
        // ^\w+(\s+\w+)*$
        var theTranscript =  jQuery('div.region-wrapper > pre').html();
        var newTranscript = theTranscript.replace(new RegExp( searchFor, "gi" ), "<span class = 'highlighted' style='background: yellow'>"+searchFor+" </span>");
        jQuery('div.region-wrapper > pre').html(newTranscript);
    }

    if (document.getElementById('islandora-solr-search-return-link')) {
        console.log('has solr search return link');
        var searchUrl = document.getElementById('islandora-solr-search-return-link').getElementsByTagName('a')[0].pathname;     // "/islandora/search/%22peace%20corps%22"
        var searchFor = searchUrl.split('/').pop();         // get the last part of the array
        var searchFor = decodeURIComponent(searchFor).trim();       // decode URL and remove leading or trailing spaces
        var searchFor = searchFor.replace(/^\"+|\"+$/g, '');        // remove leading and trailing quotes; hat tip to https://stackoverflow.com/a/32516190
        console.log(searchFor);

        if (searchFor) {
            var hasTabs = jQuery('ul.tabs li');
            if (hasTabs) {
                var hasURL = jQuery('ul.tabs li ~ li a').attr('href')
                if (hasURL) {
                    var isTranscript = hasURL.indexOf('/transcript');
                    if (isTranscript !== -1) {
                       console.log('old url', hasURL);
                        var newURL = hasURL + '?search=' + searchFor;
                        console.log('newURL', newURL);
                        jQuery('ul.tabs li ~ li a').attr('href', newURL);
                    }
                }
            }
        }
    }
})
